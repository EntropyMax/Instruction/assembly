
---

# Labs folder for Assembly

**There will be multiple platform specific labs for this module.**  
As with previous assignments, the labs will be submitted in a file titled ***Assembly***  
Unlike some other modules, you will be required to conduct unit tests.  

- For Linux, in order to install the prerequisites for unit tests, you will use the following commands:
    - `sudo apt update`
    - `sudo apt install cmake`


The completed programs will be submitted in the top level of the labs folder.   
Please use the naming convention requested - e.g. your completed Lab1.nasm will be named ***LastnameFirstinitial_Lab1.nasm*** *in the top level **labs** folder* - **not** in the Lab_1 folder.

--- 


**The Assembly Labs are located here:**

- Assembly Labs Folder [here](https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/tree/master/mdbook/src/labs)

---



