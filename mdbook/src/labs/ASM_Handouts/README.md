# The available handouts are available via the following links

* [ASM Cheatsheet](https://gitlab.com/90cos/public/training/lessons/assembly/-/blob/master/mdbook/src/ASM_Handouts/Assembly%20Cheat%20Sheet.pdf)
* [GDB Cheatsheet](https://gitlab.com/90cos/public/training/lessons/assembly/-/blob/master/mdbook/src/ASM_Handouts/GDB%20Cheat%20Sheet.pdf)
* [Lab instructions](https://gitlab.com/90cos/public/training/lessons/assembly/-/blob/master/mdbook/src/ASM_Handouts/lab_instructions.txt)
* [Sample GDB init](https://gitlab.com/90cos/public/training/lessons/assembly/-/blob/master/mdbook/src/ASM_Handouts/sample-gdbinit)