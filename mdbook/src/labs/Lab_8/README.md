


---

# Lab 8: String Calls 

Complete Lab 8, follow the instructions provided in the folders.

* You should have `git cloned` the Lab8 folder and it's contents
* Modify the *.nasm file (Each function should have a comment block - lines starting with ';' containing instructions)
* Build and run using the following commands:

```
cmake . && cmake --build .
./lab8
```



---


