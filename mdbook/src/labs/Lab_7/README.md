
---

# Lab 7: Control Flow

Proceed to Lab 7 and follow the instructions provided in the folder.
Once you have finished working on the lab you may continue to the next topic.


* You should have `git cloned` the Lab7 folder and it's contents
* Modify the *.nasm file (Each function should have a comment block - lines starting with ';' containing instructions)
* Build and run using the following commands:

```
cmake . && cmake --build .
./lab7
```


---

