

---

# Lab 9: Calling Conventions 

**Complete Lab 9, follow the instructions provided in the folders.**


* You should have `git cloned` the Lab9 folder and it's contents
* Modify the *.nasm file (Each function should have a comment block - lines starting with ';' containing instructions)
* Build and run using the following commands:

```
cmake . && cmake --build .
./lab9
```



---

**End of Assembly**
